"use client"

const label = { inputProps: {"aria-label": "Switch demo"}}

export default function Home() {
  return (
    <div className="container">
      <h1>El Observador</h1>
      <p>Bienvenido a el portal &apos;El observador&apos; </p>
    </div>
  )
}