"use client"
import React, { useEffect, useState } from 'react'
import SearchNews from '../components/SearchNews'
import { News } from '../response/news.response';
import { Auxiliar } from '../inputs/auxiliar.input';
import Card from '../components/Card';

const SearchPage = () => {
  const [news, setNews] = useState<News[]>([])
  const [body, setBody] = useState<Auxiliar>({
    criteria: '',
    param: 'title',
    order: 'ASC'
  })

  const findPosts = (data: any) => {
    setBody({ ...data })
  }

  const search = async () => {
    const res = await fetch(`http://localhost:8080/news/search?criteria=${body.criteria}&param=${body.param}&order=${body.order}`);
    const data = await res.json();
    setNews([...data]);
  }

  useEffect(() => {
    search()
  }, [body])

  return (
    <>
      <SearchNews searchNews={findPosts} />
      <div>
        {
          news.map((post) => {
            return <Card key={post.id} post={post} />
          })
        }
      </div>
    </>
  )
}

export default SearchPage
