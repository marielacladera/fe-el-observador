"use client"

import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { CreateNewsInput } from '../inputs/createNews.input';
import { User } from '../response/user.response';
import { useRouter } from 'next/navigation';

const CreateNewsPage = () => {

  const router = useRouter();
  
  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData: FormData = new FormData(event.currentTarget);
    const user: User = JSON.parse(localStorage.getItem('user')!)
    const instance: CreateNewsInput = {
      author: user.email,
      content: formData.get('content') as string,
      place: formData.get('place') as string,
      title: formData.get('title') as string
    }

    await fetch(`http://localhost:8080/news`, {
      method: 'POST',
      body: JSON.stringify(instance),
      headers: {
        'Content-Type': 'application/json'
      }
    })

    router.push('/listNews')
  };


  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                autoComplete="given-name"
                name="title"
                required
                fullWidth
                id="title"
                label="titulo"
                autoFocus
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="content"
                name="content"
                label="Contenido"
                multiline
                rows={8}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="place"
                label="Lugar"
                name="place"
                autoComplete="place"
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Crear News
          </Button>
        </Box>
      </Box>
    </Container>
  );
}

export default CreateNewsPage
