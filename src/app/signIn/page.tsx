"use client"

import { Box, Button, Container, CssBaseline, TextField, Typography } from '@mui/material';
import { useRouter } from 'next/navigation';
import React from 'react'
import { LoginInput } from '../inputs/login.input';
import { User } from '../response/user.response';

const SignInPage = () => {
      
    
    const router = useRouter();

    const handleSubmit = async(event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const formData: FormData = new FormData(event.currentTarget);

        const instance: LoginInput = {
            email: formData.get('email') as string,
            password: formData.get('password') as string
        }

        const response =  await fetch(`http://localhost:8080/login`, {
            method: 'POST',
            body: JSON.stringify(instance),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        if(!response.ok) return;
        const user: User = await response.json();
        localStorage.setItem('userId', JSON.stringify(user.id));
        localStorage.setItem('email', JSON.stringify(user.password));
        //localStorage.setItem('roleName', JSON.stringify(user.role.name));
        router.push('/')
      };

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Typography component="h1" variant="h5">
                    Iniciar Sesion
                </Typography>
                <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Correo electronico"
                        name="email"
                        autoComplete="email"
                        autoFocus
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Contrasena"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                    >
                        Sign In
                    </Button>
                </Box>
            </Box>
        </Container>
    )

}

export default SignInPage
