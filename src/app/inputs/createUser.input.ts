export interface CreateUserInput {
    email: string;
    password: string;
    roleId: number;
}