export interface CreateNewsInput {
    author: string;
    content: string;
    place: string;
    title: string;
}