export interface CreateCommentInput {
    newId: number;
    content: string;
    userId: string;
}