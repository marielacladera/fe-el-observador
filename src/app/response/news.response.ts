import { CommentResponse } from "./comment.response";

export interface News {
    id: number;
    title: string;
    postDate: Date;
    author: string;
    place: string;
    points: number;
    content: string;
    comments: CommentResponse[];
}