import { User } from "./user.response";

export interface Role {
     id: number;

    name: string;

    users: User[];
}