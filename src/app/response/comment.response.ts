export interface CommentResponse {
    id: number,
    content: string;
    postDate: Date;
    userId: string;
}