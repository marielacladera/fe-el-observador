import { Role } from "./role.response";

export interface User {
    id: number;

    email: string;

    password: string;

    role: Role;
    
}