
import React from 'react'
import { News } from '../response/news.response';
import Card from '../components/Card';

const newsList = async () => {
    const res = await fetch(`http://localhost:8080/news/search?param=title&order=ASC`);
    const data: News[] = await res.json();
    return data
}

const ListNewsPage = async () => {
    const news: News[] = await newsList()
    return (
        <div>
            {
                news.map((post) => {
                   return <Card key={post.id} post={post} />
                })
            }
        </div>
    )
}

export default ListNewsPage
