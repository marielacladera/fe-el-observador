"use client"
import Link from 'next/link'
import React, { useEffect, useState } from 'react'

const Navbar = () => {
  const [isLogged, setIsLogged] = useState(false)

  const handleStorageChange = () => {
    setIsLogged(!!localStorage.getItem('email'));
  }

  useEffect(() => {
    handleStorageChange()
  }, [isLogged])

  const handleLogOut = () => {
    localStorage.removeItem('email');
  }

  /*{ !!isLogged && <div className='personalization-nav'><Link href="/searchNews"> Buscar News</Link></div>}
        { !!isLogged && <div className='personalization-nav'><Link href="/createNews"> Crear News</Link></div>}
        { !!isLogged && <div className='personalization-nav'><a onClick={handleLogOut}> Salir</a></div>}

        {!isLogged && <div className='personalization-nav'><Link href="/register">Registrarse</Link></div>}
        {!isLogged && <div className='personalization-nav'><Link href="/signIn">Iniciar Sesion</Link></div>}
     */ 
  
  return (
    <div className='main-nav'>
      <Link href="/">El Observador</Link>
      <nav>

       <div className='personalization-nav'><Link href="/listNews"> Listar Noticias</Link></div>
       <div className='personalization-nav'><Link href="/searchNews"> Buscar Noticias</Link></div>
       <div className='personalization-nav'><Link href="/createNews"> Crear Noticia</Link></div>
       <div className='personalization-nav'><a onClick={handleLogOut}> Salir</a></div>

       <div className='personalization-nav'><Link href="/register">Registrarse</Link></div>
       <div className='personalization-nav'><Link href="/signIn">Iniciar Sesion</Link></div>
      </nav>
    </div>
  )
}

export default Navbar
