"use client"
import React, { useState } from 'react'
import { News } from '../response/news.response'
import ListComments from './ListComments'
import { useRouter } from 'next/navigation'

const Card = ({ post }: { post: News }) => {

    const router = useRouter();

    const [openPoint, setOpenPoint] = useState<boolean>(false)

    const handleOpenPoint = () => {
        setOpenPoint(!openPoint)
    }

    async function handleDeleteNews() {
        await fetch(`http://localhost:8080/news/${post.id}`, {
            method: 'DELETE',
        });
        router.refresh();
    }

    const handleSelectPoint = async(event: any) => {
        const {target} = event
        const point : number = +target.value
        await fetch(`http://localhost:8080/news/updatePoints?newId=${post.id}&point=${point}`, {
            method: 'PUT'
        })
        await new Promise((resolve) => setTimeout(resolve, 3000))
        router.refresh();
    }

    return (<div className='news-content'>
        <div key={post.id}>
            <div className='news-header'>
                <h1>{post.title} - <small>{post.author}</small></h1>
                <p>Puntaje: {post.points}</p>
            </div>
            <p>{post.content}</p>
        </div>
        <div>
            <button onClick={handleDeleteNews}>Eliminar</button>
            <button>Actualizar</button>
            {!openPoint && <button onClick={handleOpenPoint}>Calificar Noticia</button>}
            {!!openPoint &&
                <select onChange={handleSelectPoint} name="points" id="points">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            }
        </div>
        {
            <ListComments comments={post.comments} newsId={post.id} />
        }
    </div>
    )
}

export default Card
