"use client"
import { Box, Button, Grid, MenuItem, TextField } from '@mui/material'
import React from 'react'
import { Auxiliar } from '../inputs/auxiliar.input';

const SearchNews = ({searchNews}: { searchNews: (value: Auxiliar) => void }) => {

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData: FormData = new FormData(event.currentTarget);
    const instance: Auxiliar = {
      criteria:  formData.get('criteria') as string,
      param: formData.get('param') as string,
      order: formData.get('order') as string
    }
    searchNews(instance)
  };

  const parameters = [
    {
      value: 'title',
      label: 'Titulo',
    },
    {
      value: 'content',
      label: 'Contenido',
    },
    {
      value: 'author',
      label: 'Autor',
    },

  ]

  const orders = [
    {
      value: 'ASC',
      label: 'ASC',
    },
    {
      value: 'DESC',
      label: 'DESC',
    },
  ]

  return (
    <Box
      sx={{
        marginTop: 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
        <Grid container spacing={1}>
          <Grid item xs={12} sm={3}>
            <TextField
              autoComplete="given-name"
              name="criteria"
              required
              id="criteria"
              label="Criterio"
              autoFocus
              size="small"
            />
          </Grid>
          <Grid item xs={12} sm={3}>
            <TextField
              name='param'
              id="param"
              select
              label="Parametro"
              defaultValue="title"
              helperText="Selecciona tu parametro"
              size="small"
            >
              {parameters.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={12} sm={3}>
            <TextField
              id="order"
              name='order'
              select
              label="Orden"
              defaultValue="ASC"
              helperText="Selecciona el orden"
              size="small"
            >
              {orders.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={12} sm={3}>
            <Button
              type="submit"
              variant="contained"
            >
              Buscar News
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  )
}

export default SearchNews
