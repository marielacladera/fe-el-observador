import Link from 'next/link'
import React from 'react'

const NavbarElObservador = () => {
  return (
    <div className='main-nav'>
      <Link href="/">El Observador</Link>
      <nav>
        <div className='personalization-nav'><Link href="/register">Registrarse</Link></div>
        <div className='personalization-nav'><Link href="/signIn">Iniciar Sesion</Link></div>
      </nav>
    </div>

  )
}

export default NavbarElObservador
