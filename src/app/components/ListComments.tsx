"use client"

import React, { useState } from 'react'
import { CommentResponse } from '../response/comment.response'
import { CreateCommentInput } from '../inputs/create-comment.input'
import { useRouter } from 'next/navigation'

const ListComments =({ comments, newsId }: { comments: CommentResponse[], newsId: number }) => {
    const route = useRouter();

    const [inputValue, setInputValue] = useState<string>('')
    const [openInput, setOpenInput] = useState<boolean>(false)

    const onHandleOpenInput = () => {
        setOpenInput(!openInput)
    }

    const onHandleValue = ({target} : {target: any}) => {
        setInputValue(target.value);
    }

    const onSaveComment = async() => {
        const userId: string = localStorage.getItem('userId') as string;
        const instance: CreateCommentInput = {
            newId: newsId,
            content: inputValue,
            userId: userId
        };

        await fetch('http://localhost:8080/news/comments', {
            method: 'POST',
            body: JSON.stringify(instance),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(() => {
            route.push('/listNews');
        })
    }

    return (
        <>
            <div className='comments-content'>
                <h3>Comentarios</h3>
                { !!openInput && 
                    <input type="text" placeholder='Registra comentario' onChange={onHandleValue}  />
                }
                <button onClick={onHandleOpenInput}>
                    { !openInput && 'Anadir Comentario'}
                    { !!openInput && 'Cancelar'}
                </button>
                { !!inputValue.length && 
                    <button onClick={onSaveComment}>Guardar</button>
                }
                {comments.map((comment) => {
                    return <div className='comments-content-item' key={comment.id}>
                        <p>{comment.content}</p>
                    </div>
                })
                }
            </div>
        </>
    )
}

export default ListComments