import { Box } from "@mui/material"
import CardNews from "./CardNews"
import { News } from "../response/news.response"

const ListNews = ({news}: {news: News[]}) => {
    return (
        <Box
            my={4}
            display="flex"
            alignItems="center"
            justifyContent="center"
            gap={4}
            p={2}
        >
            {news.map((post) => {
                return <CardNews key={post.id} post={post}/>
            })}
        </Box>
    )
}

export default ListNews
