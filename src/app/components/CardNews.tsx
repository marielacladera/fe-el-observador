import { Card, CardActions, CardContent, CardMedia, Typography } from '@mui/material'
import React from 'react'
import { News } from '../response/news.response'

const CardNews = ({ post }: { post: News }) => {

  return (
    <>
      <Card sx={{ maxWidth: 200 }}>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {post.title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {
              post.content
            }
          </Typography>
        </CardContent>
        <CardActions>
          </CardActions>
        <CardContent>
          {post.comments.map((comment) => {
            return <Typography variant="inherit" color="text.secondary">
              {
                comment.content
              }
            </Typography>

          })}
        </CardContent>
      </Card>
    </>
  )
}

export default CardNews
