"use client"
import { Button, Card, CardActions, CardContent, Typography } from '@mui/material'
import React from 'react'
import { News } from '../response/news.response'
import { useRouter } from "next/navigation";


const CardWithOptions = ({ post }: { post: News }) => {

    const router = useRouter();

    const onHandleDeleteNews = async () => {
        await fetch(`http://localhost:8080/news/${post.id}`, {
            method: 'DELETE',
        });
        router.push('/listNews')
    }

    return (
        <>
            <Card sx={{ maxWidth: 300 }}>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {post.title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary" >
                        Autor: {post.author} Lugar: {post.place}
                    </Typography>
                    <Typography variant="inherit" component="div">
                        {
                            post.content
                        }
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" onClick={onHandleDeleteNews}>Actualizar</Button>
                </CardActions>
            </Card>
        </>
    )
}

export default CardWithOptions
